#!/bin/sh

# Stop on first failure
set -x

# Show each command
set -x

# Exit success if container is already created
if docker exec grafana sh -c exit 2>/dev/null; then
    echo >&2 "INFO: grafana container is already created"
    exit 0
fi

# Run docker container
docker \
    run \
        `# Set name of newly created container` \
        --name grafana \
        \
        `# Specify a timezone to use Warsaw` \
        --env TZ=Europe/Warsaw \
        \
        `# Expose web ui` \
        --publish 3000:3000 \
        \
        `# Data` \
        --volume /media/storage/docker/grafana/data:/var/lib/grafana \
        \
        `# Always restart container, except that it is stopped manually` \
        `# Then even when docker deamon restarts it will not start this container` \
        --restart unless-stopped \
        \
        `# Run in background` \
        --detach \
        \
        `# Image to run` \
        `# There is known regression in latest grafana 6.4.3 for arm64` \
        `# https://github.com/grafana/grafana/issues/19585` \
        `# grafana/grafana` \
        grafana/grafana-arm64v8-linux:dev-musl \
