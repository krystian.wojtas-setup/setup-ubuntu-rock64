#!/bin/sh

# Stop on first failure
set -x

# Show each command
set -x

# Exit success if container is already created
if docker exec nextcloud sh -c exit 2>/dev/null; then
    echo >&2 "INFO: nextcloud container is already created"
    exit 0
fi

# Install secret configuration
pass \
    show \
    rock64/nextcloud/config \
    > /media/storage/docker/nextcloud/config/www/nextcloud/config/config.php

# Run docker container
docker \
    run \
        `# Set name of newly created container` \
        --name nextcloud \
        \
        `# Set container process user and group id` \
        --env PUID=`id --user www-data` \
        --env PGID=`id --group www-data` \
        \
        `# Specify a timezone to use Warsaw` \
        --env TZ=Europe/Warsaw \
        \
        `# Expose web ui by secure connection` \
        --publish 444:443 \
        \
        `# Nextcloud configs` \
        --volume /media/storage/docker/nextcloud/config:/config \
        \
        `# Nextcloud personal data` \
        --volume /media/storage/docker/nextcloud/data:/data \
        \
        `# Always restart container, except that it is stopped manually` \
        `# Then even when docker deamon restarts it will not start this container` \
        --restart unless-stopped \
        \
        `# Run in background` \
        --detach \
        \
        `# Image to run` \
        `# Linuxserver container is based on nginx` \
        linuxserver/nextcloud
